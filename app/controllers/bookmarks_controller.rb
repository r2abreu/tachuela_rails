class BookmarksController < ApplicationController
  layout 'dashboard'
  before_action :redirect_unauthenticated

  def index
    user = User.find(session[:user_id])
    @bookmarks = user.bookmarks
  end

  def show
    user = User.find(session[:user_id])
    @bookmark = user.bookmarks.find(params[:id])
  end

  def new 
    user = User.find(session[:user_id])
    @bookmark = user.bookmarks.build
  end

  def create
    user = User.find(session[:user_id])
    @bookmark = user.bookmarks.build(bookmark_params)
    if @bookmark.save
      flash[:notice] = "Bookmark was created successfully"
      redirect_to @bookmark
    else 
      render :new, status: :unprocessable_entity
    end
  end


  def edit
    user = User.find(session[:user_id])
    @bookmark = user.bookmarks.find(params[:id])
  end

  def update
    user = User.find(session[:user_id])
    @bookmark = user.bookmarks.find(params[:id])

    if @bookmark.update(bookmark_params);
      flash[:notice] = "Bookmark was updated successfully"
      redirect_to @bookmark
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy 
    @bookmark = Bookmark.find(params[:id])
    @bookmark.destroy

    redirect_to bookmarks_path, status: :see_other
  end

  private
    def bookmark_params
      params.require(:bookmark).permit(:title, :description, :url)
    end
    
    def redirect_unauthenticated
      redirect_to start_path unless session[:user_id]
    end
end