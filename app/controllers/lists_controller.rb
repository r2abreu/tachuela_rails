class ListsController < ApplicationController
  layout 'dashboard'
  before_action :redirect_unauthenticated
  
  def index
    user = User.find(session[:user_id])
    @lists = user.lists
  end

  def show
    user = User.find(session[:user_id])
    @lists = user.lists
  end

  def new
    @list = List.new
    @list.items.build
  end

  def create
    user = User.find(session[:user_id])
    @list = user.lists.new(list_params)

    if user.save       
      flash[:notice] = "List saved"
      redirect_to @list
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @list = List.find(params[:id])
  end

  def update
    @list = List.find(params[:id])
    if @list.update(list_params)
      flash[:notice] = "List was updated succesfuly"
      redirect_to @list
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
  end

  private
    def redirect_unauthenticated
      redirect_to start_path unless session[:user_id]
    end

    def list_params
      params.require(:list).permit(:title, items_attributes: [:id, :content, :_destroy])
    end
end
