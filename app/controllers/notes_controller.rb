class NotesController < ApplicationController
  layout 'dashboard'
  before_action :redirect_unauthenticated
  
  def index
    user = User.find(session[:user_id])
    @notes = user.notes
  end

  def show
    user = User.find(session[:user_id])
    @note = user.notes.find(params[:id])
  end

  def new
    user = User.find(session[:user_id])
    @note = user.notes.build
  end

  def create
    user = User.find(session[:user_id])
    @note = user.notes.build(note_params)

    if user.save
      flash[:notice] = "Note saved correctly"
      redirect_to @note
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    user = User.find(session[:user_id])
    @note = user.notes.find(params[:id])
  end

  def update
    user = User.find(session[:user_id])
    @note = user.notes.find(params[:id])

    if @note.update(note_params)
      flash[:notice] = "Note updated correctly"
      redirect_to @note
    else
      flash.now[:alert] = "Unable to update note"
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    user = User.find(session[:user_id])
    note = user.notes.find(params[:id])

    note.destroy

    redirect_to notes_path, status: :see_other
  end

  private
    def note_params
      params.require(:note).permit(:title, :description)
    end
    
    def redirect_unauthenticated
      redirect_to start_path unless session[:user_id]
    end
end
