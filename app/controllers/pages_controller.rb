class PagesController < ApplicationController
  layout 'pages'
  
  def home
  end

  def start
  end

  def start_user
    redirect_to in_or_up_path(user_params[:username])
  end

  def in_or_up
    @user = User.find_by(username: params[:username]) 
    @requested_username = params[:username]
  end

  def dashboard
    redirect_to start_path unless session[:user_id] # Why I cannot access application helper: current_user?
  end

  private
    def user_params
      params.require(:user).permit([:username])
    end
end