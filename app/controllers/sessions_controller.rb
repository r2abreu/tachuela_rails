class SessionsController < ApplicationController
  def new
    redirect_to user_path(session[:user_id]) if session[:user_id] 
  end

  def create
    @session = session_params
    username = @session[:username].downcase
    @user = User.find_by(username: username)
    @requested_username = username
    
    if @user && @user.authenticate(session_params[:password])
      flash[:notice] = "Logged in successfully"
      session[:user_id] = @user.id
      redirect_to :root
    else
      flash.now[:alert] = "Your login details were incorrect"
      render 'pages/in_or_up', status: :unprocessable_entity, layout: 'pages'
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:notice] = "Logged out successfully"
    redirect_to signin_path, status: :see_other
  end

  private
    def session_params
      params.require(:session).permit(:username, :password)
    end
end