class UsersController < ApplicationController
  # before_action :set_user -> Remove possible redundacy 
  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_param)
    if @user.save
     session[:user_id] = @user.id
     flash[:notice] = "Hello #{@user.username}, welcome to Tachuela"
     redirect_to :root 
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @user = User.find(params[:id])
  end 

  private
    def user_param 
      params.require(:user).permit(:username, :password)
    end
end 