class Bookmark < ApplicationRecord
  validates_presence_of :title, :description, :url
  belongs_to :user
end