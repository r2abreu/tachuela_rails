class List < ApplicationRecord
  has_many :items
  belongs_to :user
  validates :title,
    presence: true

  accepts_nested_attributes_for :items, allow_destroy: true
end