class User < ApplicationRecord
  before_save { self.username = username.downcase }
  has_secure_password
  has_many :bookmarks
  has_many :notes
  has_many :lists

  validates :username, 
    presence: true, 
    uniqueness: { case_sensitive: false }, 
    length: { minimum: 3, maximum: 25 }
end
