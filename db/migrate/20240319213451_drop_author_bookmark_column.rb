class DropAuthorBookmarkColumn < ActiveRecord::Migration[7.1]
  def change
    remove_column :bookmarks, :author
  end
end
