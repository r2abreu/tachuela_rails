class AddUserToBookmark < ActiveRecord::Migration[7.1]
  def change
    add_column :bookmarks, :user_id, :int
  end
end
