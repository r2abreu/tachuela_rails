class CreateList < ActiveRecord::Migration[7.1]
  def change
    create_table :lists do |t|
      t.integer :user_id
      t.string :title
      t.timestamps
    end
  end
end
