class AddListIdToItems < ActiveRecord::Migration[7.1]
  def change
    add_column :items, :list_id, :int
  end
end
