require "test_helper"

class BookmarkTest < ActiveSupport::TestCase
  def setup
    @user = User.new(username: "Unique", password: "12345")
    @bookmark = @user.bookmarks.build(
      title: "Foo", 
      description: "FooDescription", 
      url: "www.foo.com"
    )
  end

  test "title can't be blank" do
    @bookmark.title = ""
    assert_not @bookmark.valid?
  end
  test "description can't be blank" do
    @bookmark.description = ""
    assert_not @bookmark.valid?
  end
  test "url can't be blank" do
    @bookmark.url = ""
    assert_not @bookmark.valid?
  end
end