require "test_helper"

class ItemTest < ActiveSupport::TestCase
  def setup
    @user = User.new(username: "User", password: "12345")
    @list = @user.lists.build(title: "Foo")
    @item = @list.items.build(content: "Foo")
  end

  test "content can't be blank" do
    @item.content = ""
    assert_not @item.valid?
  end
end