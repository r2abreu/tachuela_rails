require "test_helper"

class ListTest < ActiveSupport::TestCase
  def setup
    @user = User.new(username: "Unique", password: "1234")
    @list = @user.lists.build(title: "Foo")
  end

  test "title is not blank" do
    @list.title = ""
    assert_not @list.valid?
  end
end
