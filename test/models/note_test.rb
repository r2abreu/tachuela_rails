require "test_helper"

class NoteTest < ActiveSupport::TestCase
  def setup
    @user = User.new(username: "Unique", password: "1234")
    @note = @user.notes.build(title: "Title", description: "Desc")
  end

  test "title is not blank" do
    @note.title = ""
    assert_not @note.valid?
  end

  test "description is not blank" do
    @note.description = ""
    assert_not @note.valid?
  end
end