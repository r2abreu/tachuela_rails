require "test_helper"

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(username: "Foo", password: "12345")
  end

  test "username can't be blank" do
   @user.username = ""
   @user.password = "1234"

   assert_not @user.valid?
  end

  test "username is unique" do
   @user.save
   
   @user2 = User.new(username: "Foo", password: "1234")
   assert_not @user2.valid?
  end

  test "username is not too long" do
    MAX_LENGTH = 25
    @user.username = "a" * (MAX_LENGTH + 1)
    
    assert_not @user.valid?
  end

  test "username is not too short" do 
    MIN_LENGTH = 3
    @user.username = "a" * (MIN_LENGTH - 1)

    assert_not @user.valid?
  end
end